<?php

class Main{

	public function handleTransaction(Transaction $transactionType){
		$transactionType->processTransactions();
	}

}

// transaction interface
interface Transaction{

	// read json
	public function readJson();

	// write to db
	public function writeDb();

	// process transactions
	public function processTransactions();

}

class One implements Transaction{

	protected $transactions = [];

	// read json
	public function readJson(){
		$this->transactions = json_decode(file_get_contents("transactions/one.json"), true);
	}
	// write to db
	public function writeDb(){
		$db = [];
		$count = 0;
		foreach($this->transactions as $transaction){
			// id
			$db[$count]['id'] = $transaction['id'];
			// body
			$db[$count]['body'] = $transaction['body'];
			// amount - needs to be an absolute value
			$db[$count]['amount'] = abs($transaction['amount']);
			// date
			$db[$count]['date'] = $transaction['added_at'];
			// date needs to be converted to one standard
			#code...

			// type - credit or debit
			// if amount positive = debit, else = credit
			$type = ($transaction['amount'] < 0) ? 'credit' : 'debit';
			$db[$count]['type'] = $type;
			$count++;
		}
		echo json_encode($db).PHP_EOL;
	}
	public function processTransactions(){
		$this->readJson();
		$this->writeDb();
	}
}

class Two implements Transaction{
	// read json
	public function readJson(){
		echo "Reading JSON from Two".PHP_EOL;
	}

	// write to db
	public function writeDb(){
		echo "Writing to DB from Two".PHP_EOL;
	}

	public function processTransactions(){
		$this->readJson();
		$this->writeDb();
	}
}

class Three implements Transaction{
	// read json
	public function readJson(){
		echo "Reading JSON from Three".PHP_EOL;
	}

	// write to db
	public function writeDb(){
		echo "Writing to DB from Three".PHP_EOL;
	}

	public function processTransactions(){
		$this->readJson();
		$this->writeDb();
	}
}


$one = new One();
$two = new Two();
$three = new Three();

$transactions = [$one, $two, $three];

$main = new Main();

foreach($transactions as $transaction){
	$main->handleTransaction($transaction);
}















